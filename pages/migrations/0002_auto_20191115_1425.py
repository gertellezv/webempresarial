# Generated by Django 2.2.7 on 2019-11-15 19:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='page',
            old_name='conent',
            new_name='content',
        ),
    ]
