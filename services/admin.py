from django.contrib import admin
from services.models import Service
# Register your models here.

class Serviceadmin(admin.ModelAdmin):
    list_display = ('title','subtitle','updated','created')
    list_display_links = ('title','subtitle')
    
    fieldsets = (
        ('Servicio', {
            'fields':(('title','subtitle'),'image','content','updated','created')
        }),
    )
    readonly_fields = ('updated','created')
admin.site.register(Service, Serviceadmin)
