from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.urls import reverse
from .forms import ContactForm

# Create your views here.

def contact(request):
    contact_form = ContactForm()

    if request.method == 'POST':
        contact_form = ContactForm(data=request.POST)
        if contact_form.is_valid():
            name = request.POST.get('name','')
            email = request.POST.get('email','')
            content = request.POST.get('content','')
            
            # Datos para enviar por correo

            #Asunto
            subject = 'Formulario de contacto recibido'
            #Contenido del mensaje
            message = """
            Hola {0}, hemos recibido tu mensaje, pronto nos pondremos en contacto.
            \n\n A continuación puedes ver el contenido de tu mensaje:
            \nNombre: {1}
            \nCorreo: {2}
            \nMensaje: {3}
            \n\n No hace falta responder a este mensaje.
            """.format(name,name,email,content)
            # Remitente
            mail_from = 'gertellezv@gmail.com'
            # Destinatarios
            mail_to = [email]
            # Enviar el correo
            send_mail(subject,message,mail_from,mail_to,fail_silently=False)
            
            # Suponemos que todo ha ido bien
            return redirect(reverse('contact')+'?ok')

    return render(request,'contact/contact.html',{'form':contact_form})

